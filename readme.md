# Google Hangouts Chat enhancements

## Features

 - Several themes (including dark and compact ones)
 - Improved tray icon support (closing window doesn't terminate app)

## Installation

### Windows

 - open ```%LOCALAPPDATA%\Google\Hangouts Chat\resources\app\```
 - copy ```enhancements``` folder (into opened app folder)
 - backup ```main.js``` (optional but *recommended*)
 - append ```require("./enhancements");``` to the end of ```main.js``` file

Note that last step needs to be repeated after each application update (as installer will overwrite ```main.js```).

## License

Project is licensed under MIT license. Some parts originate from other projects - see Credits section.

## Credits

 - [Better Hangouts Chat](https://github.com/paveyry/better-hangoutschat), Pierre-Alexandre VEYRY, [MIT license](https://github.com/paveyry/better-hangoutschat/blob/master/LICENSE.txt) - compact themes
 - [Dark chat.google.com](https://userstyles.org/styles/166023/dark-chat-google-com); Jiří Kremser, CC BY - Creative Commons Attribution - dark theme