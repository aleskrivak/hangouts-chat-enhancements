var electron = require("electron");
var fs = require('fs');
var path = require('path');
var debug = true;

// Store tray object reference by monkey-patching
var originalOn = electron.Tray.prototype.on;
var applicationTray = null;
electron.Tray.prototype.on = function() {
    applicationTray = this;
    originalOn.apply(this, arguments);
};

// Error method
var errorMessage = function(message) {
    if (debug) {
        electron.dialog.showErrorBox("error", "" + message);
    }
}

// Load all themes
var themesPromise = new Promise(function(resolve, reject) {
    fs.readdir(path.join(__dirname, "themes"), { withFileTypes: true }, function(err, dir) {
        if (err) {
            reject(err);
            return;
        }
        var themes = [];
        for (const entry of dir) {
            if (entry.isFile() && entry.name.toLowerCase().endsWith(".css")) {
                const name = entry.name.substring(0, entry.name.length - 4);
                const cssPath = path.join(__dirname, "themes", entry.name);
                themes.push(new Promise((resolve, reject) => {
                    fs.readFile(cssPath, "utf8", (err, data) => {
                        if (err) {
                            reject(err);
                            return;
                        }
                        resolve({
                            name: name,
                            css: data.replace(/'/g, "\"").replace(/\r?\n|\r/g, " ")
                        });
                    });
                }));
            }
        }
        resolve(Promise.all(themes));
    });
}).catch((err) => { errorMessage(err); return []; });

// Load config
const configPath = path.join(electron.app.getPath("userData"), "hangouts-enhancements-config.json");
const defaultConfig = {
    theme: "dark-compact",
    closeBehavior: "tray"
};
var configPromise = new Promise((resolve, reject) => {
    fs.readFile(configPath, "utf8", (err, data) => {
        if (err) {
            reject(err);
            return;
        }
        try {
            resolve(JSON.parse(data));
        } catch (error) {
            reject(error);
        }
    });
}).catch(() => defaultConfig);

var extendApp = function(config, themes) {
    // Writing of configuration
    var updateConfig = function() {
        try {
            fs.writeFileSync(configPath, JSON.stringify(config));
        } catch (error) {
            errorMessage(error);
        }
    }

    // Prepend handler to close event to allow hiding in tray
    var allWindows = electron.BrowserWindow.getAllWindows();
    for (var i = 0; i < allWindows.length; ++i) {
        if (!allWindows[i].autoHideMenuBar) {
            allWindows[i].prependListener("close", function(event){
                if (!this.terminateApplication) {
                    if (config.closeBehavior == "minimize") {
                        event.preventDefault();
                        this.minimize();
                    } else if (config.closeBehavior == "tray") {
                        event.preventDefault();
                        this.hide();
                    }
                }
            });
        }
    }

    // Add tray icon context menu
    var contextMenu = electron.Menu.buildFromTemplate([
        {
            label: "Open",
            click: function() {
                applicationTray.emit("click");
            }
        },
        {
            label: "Quit",
            click: function() {
                var allWindows = electron.BrowserWindow.getAllWindows();
                for (var i = 0; i < allWindows.length; ++i) {
                allWindows[i].terminateApplication = true;
                }
                electron.app.quit();
            }
        }
    ]);
    applicationTray.setContextMenu(contextMenu);

    // Prepare themes menu
    var currentCss = null;
    var setTheme = function(name, css) {
        config.theme = name;
        currentCss = css;
        updateConfig();
        electron.BrowserWindow.getFocusedWindow().webContents.reload();
    };
    var themesMenu = [
        { label: "default", type: "radio", click: () => { setTheme("default", null); } }
    ];
    for (const theme of themes) {
        const selected = config.theme == theme.name;
        if (selected) {
            currentCss = theme.css;
        }
        themesMenu.push({ label: theme.name, type: "radio", checked: selected, click: () => { setTheme(theme.name, theme.css); } });
    }

    // Close button behavior
    const setCloseBehavior = function(self) {
        config.closeBehavior = self.id;
        updateConfig();
    };
    const closeBehaviorMenu = [
        { label: "Minimize to taskbar", id: "minimize", type: "radio", click: setCloseBehavior },
        { label: "Minimize to tray", id: "tray", type: "radio", click: setCloseBehavior },
        { label: "Terminate app", id: "close", type: "radio", click: setCloseBehavior }
    ];
    for (const menuItem of closeBehaviorMenu) {
        menuItem.checked = config.closeBehavior == menuItem.id;
    }

    // Opens chat directly to circumvent deprecation message
    const forceOpenChat = function(self) {
        electron.BrowserWindow.getFocusedWindow().loadURL("https://chat.google.com");
    };

    // Update main menu
    const originalMenu = electron.Menu.getApplicationMenu();
    const modifiedMenu = []
    for (const menuItem of originalMenu.items) {
        modifiedMenu.push(menuItem);
    }
    modifiedMenu.push({ label: "Extra", submenu: [
        { label: "Themes", submenu: themesMenu },
        { label: "Close behavior", submenu: closeBehaviorMenu },
        { label: "Force open chat", id: "forceChat", click: forceOpenChat }
    ]});
    electron.Menu.setApplicationMenu(electron.Menu.buildFromTemplate(modifiedMenu));

    // Insert css styles for selected theme
    for (var i = 0; i < allWindows.length; ++i) {
        allWindows[i].webContents.on("did-finish-load", function() {
            if (currentCss) {
                this.executeJavaScript("(function() { var s = document.createElement('style');s.type = 'text/css';s.id = 'extra-theme-custom-css';s.innerHTML = '" + currentCss + "';document.head.appendChild(s); })()");
            }
            if (electron.BrowserWindow.getFocusedWindow().getURL().startsWith("https://chat.google.com/error/app-no-access/")) {
                forceOpenChat();
            }
        });
    }
};

// Wait till application is ready
electron.app.on("ready", function() {
    // Main initialization is done in promise, so we need to wait till it finishes creating windows
    var task = setInterval(function() {
        if (electron.BrowserWindow.getAllWindows().length > 1) {
            clearInterval(task);
            Promise.all([configPromise, themesPromise]).then((args) => extendApp.apply(null, args));
        }
    }, 100);
});

